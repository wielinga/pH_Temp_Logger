import sys
import csv
import math

from calibration import *

csvfile = open(sys.argv[1], 'r')
outputfile = open('processed_' + sys.argv[1], 'w')
reader = csv.reader(csvfile)
writer = csv.writer(outputfile)

def ph_from_voltage(value):
    return PH_MULTIPLIER * value + PH_ADDITION

def abstemp_cma(value):
    Rfixed = 20000.0
    Vin = 5.0

    Vout = Vin * (value / 1023.0)
    Rntc = (Vout * Rfixed) / (Vin - Vout)
    logR = math.log(Rntc)
    T = 1 / (K0 + (K1 * logR) + (K2 * logR * logR * logR))
    return T-273.15

writer.writerow(['Time', 'pH', 'Temp'])

if len(sys.argv) > 2:
    average = int(sys.argv[2])
    idx = 0
    pHVoltage = 0.0
    tempVoltage = 0.0
    for row in reader:
        pHVoltage += float(row[1])
        tempVoltage += float(row[3])
        idx += 1
        if idx >= average:
            writer.writerow([row[0], ph_from_voltage(pHVoltage / average), abstemp_cma(tempVoltage / average)])
            pHVoltage = 0.0
            tempVoltage = 0.0
            idx = 0
else:
    for row in reader:
        pHVoltage = float(row[1])
        tempVoltage = float(row[3])
        writer.writerow([row[0], ph_from_voltage(pHVoltage), abstemp_cma(tempVoltage)])
