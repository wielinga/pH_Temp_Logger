#include <RTClib.h>
#include <LiquidCrystal_I2C.h>
#include <SdFat.h>
#include <EEPROM.h>

#include "pins.h"
#include "calibration.h"

#define UI_TIMEOUT 10

#define two_decimals(value) (value < 0.0 ? (int)(value * -100) % 100 : (int)(value * 100) % 100)
#define one_decimal(value) (value < 0.0 ? (int)(value * -10) % 10 : (int)(value * 10) % 10)
#define sqr(value) (value * value)

// void linreg(int n, const double x[], const double y[], double* m, double* b, double* r){
//   double sumx = 0.0;
//   double sumx2 = 0.0;
//   double sumxy = 0.0;
//   double sumy = 0.0;
//   double sumy2 = 0.0;

//   for (int i = 0; i < n; i++){ 
//     sumx += x[i];
//     sumx2 += sqr(x[i]);
//     sumxy += x[i] * y[i];
//     sumy += y[i];
//     sumy2 += sqr(y[i]);
//   }

//   double denom = (n * sumx2 - sqr(sumx));

//   *m = (n * sumxy - sumx * sumy) / denom;
//   *b = (sumy * sumx2 - sumx * sumxy) / denom;
// }

double ph_from_voltage(double value){
    return PH_MULTIPLIER * value + PH_ADDITION;
}

double abstemp_cma(double value){
    double Vout = VIN * (value / 1023.0);
    double Rntc = (Vout * RFIXED) / (VIN - Vout);
    double logR = log(Rntc);
    double T = 1.0 / (K0 + (K1 * logR) + (K2 * logR * logR * logR));
    return T - 273.15;
}

LiquidCrystal_I2C lcd(0x27, 16, 2);

RTC_DS1307 rtc;

volatile uint16_t log_interval = (EEPROM.read(0) << 8) | EEPROM.read(1);
volatile bool trigger_second;
void every_second(){
  trigger_second = true;
}

char print_buf[64] = {0};

int pH_voltage, temp_voltage;
double pH, temp;

SdFat sd;
SdFile logFile;
bool sd_mounted = false;

DateTime timeNow;
int logFileDay;

void openLogFile(){
    if(logFile.isOpen()) logFile.close();
    logFileDay = timeNow.day();
    sprintf(print_buf, "%d-%02d-%02d.csv", timeNow.year(), timeNow.month(), timeNow.day());

    if(!logFile.open(print_buf, O_WRONLY | O_CREAT | O_AT_END)){
      lcd.clear();
      lcd.home();
      lcd.print("Unable to open");
      lcd.setCursor(0, 1);
      lcd.print("log file");
      while(1);
    }
}

uint8_t up_arrow[] = {
	0b00100,
	0b01110,
	0b10101,
	0b00100,
	0b00100,
	0b00100,
	0b00100,
	0b00100
};

uint8_t down_arrow[] = {
	0b00100,
	0b00100,
	0b00100,
	0b00100,
	0b00100,
	0b10101,
	0b01110,
	0b00100
};

const char *controls[] = {
  "     \x01    \x02",
  "   Back  Change",
  "   Back  Read",
  "year \x02    \x01",
  "mon  \x02    \x01",
  "day  \x02    \x01",
  "hour \x02    \x01",
  "min  \x02    \x01",
  "sec  \x02    \x01",
  "      Back"
};


const char *main_menu[] = {
  ">Logging",
  ">Calibrate pH",
  ">Measure pH",
  ">Measure temp",
  ">Set date",
  ">Set time",
  ">Log file status",
  ">Mount SD card",
  ">Unmount SD card"
};
const int menu_size = (sizeof(main_menu) / sizeof(char**));
int menu_index = 0;

const char *submenus[] = {
  "Interval: ",
  "Date: ",
  "Time: "
};

// false: main meu
// true: submenu
bool ui_state = false;

void print_main_menu(){
  lcd.clear();
  lcd.home();
  lcd.print(main_menu[menu_index]);
  lcd.setCursor(0, 1);
  lcd.print(controls[0]);
}

bool is_leap_year(int year){
  if(year % 4 == 0){
    if(year % 100 == 0){
      if(year % 400 == 0)
        return true;
      else
        return false;
    }else
      return true;
  }
  return false;
}

int month_length(int year, uint8_t month){
  switch(month){
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return 31;
    case 4:
    case 6:
    case 9:
    case 11:
      return 30;
    case 2:
      if(is_leap_year(year))
        return 29;
      else
        return 28;    
    default:
      return 0;
  }
}

uint8_t changing_setting = 0;
uint8_t log_minutes, log_seconds;
int changing_year;
uint8_t changing_month, changing_day, changing_hour, changing_minute, changing_second;

// process a keypress in a submenu
void submenu_keypress(int button){
  switch(menu_index){
    case 0:
      {
        bool return_to_main = false;
        lcd.clear();
        lcd.setCursor(0, 1);
        if(button == 0){
          log_minutes = log_interval / 60;
          log_seconds = log_interval % 60;
          lcd.print(controls[1]);
        }else{
          if(changing_setting){
            if(button == 3){
              if(changing_setting < 2){
                changing_setting = 2;
                lcd.print(controls[8]);
              }else{
                changing_setting = 0;
                log_interval = log_minutes * 60 + log_seconds;
                EEPROM.write(0, log_interval >> 8);
                EEPROM.write(1, log_interval & 0xFF);
                lcd.print(controls[1]);
              }
            }else{
              if(changing_setting < 2){
                if(button == 1){
                  if(log_minutes > 0)
                    log_minutes--;
                  else
                    log_minutes = 120;
                }
                if(button == 2){
                  if(log_minutes < 120)
                    log_minutes++;
                  else
                    log_minutes = 0;
                }
                lcd.print(controls[7]);
              }else{
                if(button == 1){
                  if(log_seconds > 0)
                    log_seconds--;
                  else
                    log_seconds = 59;
                }
                if(button == 2){
                  if(log_seconds < 59)
                    log_seconds++;
                  else
                    log_seconds = 0;
                }
                lcd.print(controls[8]);
              }
            }
          }else{
            if(button == 1){
              ui_state = 0;
              print_main_menu();
              return_to_main = true;
            }else{
              changing_setting = 1;
              lcd.print(controls[7]);
            }
          }
        }
        if(!return_to_main){
          lcd.home();
          lcd.print(submenus[0]);
          sprintf(print_buf, "%03u:%02u", log_minutes, log_seconds);
          lcd.print(print_buf);
        }
      }
      break;
    case 1:
      {
        if(button == 1){
          ui_state = 0;
          print_main_menu();
        }else{
          sprintf(print_buf, "pH Voltage: %u", analogRead(PH_PIN));
          lcd.clear();
          lcd.home();
          lcd.print(print_buf);
          lcd.setCursor(0, 1);
          lcd.print(controls[2]);
        }
      }
      break;
    case 2:
      {
        if(button == 1){
          ui_state = 0;
          print_main_menu();
        }else{
          double pH = ph_from_voltage(analogRead(PH_PIN));
          
          sprintf(print_buf, "pH: %d.%02d", (int) pH, two_decimals(pH));
          lcd.clear();
          lcd.home();
          lcd.print(print_buf);
          lcd.setCursor(0, 1);
          lcd.print(controls[2]);
        }
      }
      break;
    case 3:
      {
        if(button == 1){
          ui_state = 0;
          print_main_menu();
        }else{
          double temp = abstemp_cma(analogRead(TEMP_PIN));
          
          sprintf(print_buf, "Temp: %d.%02d", (int) temp, one_decimal(temp));
          lcd.clear();
          lcd.home();
          lcd.print(print_buf);
          lcd.setCursor(0, 1);
          lcd.print(controls[2]);
        }
      }
      break;
    case 4:
      {
        bool return_to_main = false;
        lcd.clear();
        lcd.setCursor(0, 1);
        if(button == 0){
          changing_year = timeNow.year();
          changing_month = timeNow.month();
          changing_day = timeNow.day();
          lcd.print(controls[1]);
        }else{
          if(changing_setting){
            if(button == 3){
              if(changing_setting < 3){
                changing_setting++;
                lcd.print(controls[2+changing_setting]);
              }else{
                changing_setting = 0;
                timeNow = DateTime(changing_year, changing_month, changing_day, timeNow.hour(), timeNow.minute(), timeNow.second());
                rtc.adjust(timeNow);
                lcd.print(controls[1]);
              }
            }else{
              lcd.print(controls[2+changing_setting]);
              if(changing_setting == 1){
                if(button == 1){
                  if(changing_year > 2000)
                    changing_year--;
                  else
                    changing_year = 2099;
                }
                if(button == 2){
                  if(changing_year < 2099)
                    changing_year++;
                  else
                    changing_year = 2000;
                }
              }else if(changing_setting == 2){
                if(button == 1){
                  if(changing_month > 1)
                    changing_month--;
                  else
                    changing_month = 12;
                }
                if(button == 2){
                  if(changing_month < 12)
                    changing_month++;
                  else
                    changing_month = 1;
                }
              }else{
                int days_in_month = month_length(changing_year, changing_month);                
                if(button == 1){
                  if(changing_day > 1)
                    changing_day--;
                  else
                    changing_day = days_in_month;
                }
                if(button == 2){
                  if(changing_day < days_in_month)
                    changing_day++;
                  else
                    changing_day = 1;
                }
              }
            }
          }else{
            if(button == 1){
              ui_state = 0;
              print_main_menu();
              return_to_main = true;
            }else{
              changing_setting = 1;
              lcd.print(controls[2+changing_setting]);
            }
          }
        }
        if(!return_to_main){
          lcd.home();
          lcd.print(submenus[1]);
          sprintf(print_buf, "%04d-%02u-%02u", changing_year, changing_month, changing_day);
          lcd.print(print_buf);
        }
      }
      break;
    case 5:
      {
        bool return_to_main = false;
        lcd.clear();
        lcd.setCursor(0, 1);
        if(button == 0){
          changing_hour = timeNow.hour();
          changing_minute = timeNow.minute();
          changing_second = timeNow.second();
          lcd.print(controls[1]);
        }else{
          if(changing_setting){
            if(button == 3){
              if(changing_setting < 3){
                changing_setting++;
                lcd.print(controls[5+changing_setting]);
              }else{
                changing_setting = 0;
                timeNow = DateTime(timeNow.year(), timeNow.month(), timeNow.day(), changing_hour, changing_minute, changing_second);
                rtc.adjust(timeNow);
                lcd.print(controls[1]);
              }
            }else{
              lcd.print(controls[5+changing_setting]);
              if(changing_setting == 1){
                if(button == 1){
                  if(changing_hour > 0)
                    changing_hour--;
                  else
                    changing_hour = 23;
                }
                if(button == 2){
                  if(changing_hour < 23)
                    changing_hour++;
                  else
                    changing_hour = 0;
                }
              }else if(changing_setting == 2){
                if(button == 1){
                  if(changing_minute > 0)
                    changing_minute--;
                  else
                    changing_minute = 59;
                }
                if(button == 2){
                  if(changing_minute < 59)
                    changing_minute++;
                  else
                    changing_minute = 0;
                }
              }else{           
                if(button == 1){
                  if(changing_second > 0)
                    changing_second--;
                  else
                    changing_second = 59;
                }
                if(button == 2){
                  if(changing_second < 59)
                    changing_second++;
                  else
                    changing_second = 0;
                }
              }
            }
          }else{
            if(button == 1){
              ui_state = 0;
              print_main_menu();
              return_to_main = true;
            }else{
              changing_setting = 1;
              lcd.print(controls[5+changing_setting]);
            }
          }
        }
        if(!return_to_main){
          lcd.home();
          lcd.print(submenus[2]);
          sprintf(print_buf, "%02u:%02u:%02u", changing_hour, changing_minute, changing_second);
          lcd.print(print_buf);
        }
      }
      break;
    case 6:
      {
        if(button == 1){
          ui_state = 0;
          print_main_menu();
        }else{
          lcd.clear();
          lcd.home();
          if(sd_mounted && logFile){
            uint32_t size =
            sprintf(print_buf, "Size: %u KiB", logFile.fileSize() >> 10);
            lcd.print(print_buf);
          }else{
            lcd.print("Card not mounted");
          }
          lcd.setCursor(0, 1);
          lcd.print(controls[2]);
        }
      }
      break;
    case 7:
      {
        if(button == 0){
          lcd.clear();
          lcd.home();
          if(sd_mounted){
            lcd.print("Already mounted");
          }else{
            sd_mounted = sd.begin(SD_CS, SPI_HALF_SPEED);
            if(sd_mounted){
              openLogFile();
              lcd.print("SD card mounted");
            }
            else
              lcd.print("Failed to mount");              
          }
          lcd.setCursor(0, 1);
          lcd.print(controls[9]);
        }else{
          ui_state = 0;
          print_main_menu();
        }
      }
      break;
    case 8:
      {
        if(button == 0){
          lcd.clear();
          lcd.home();
          if(sd_mounted){
            sd_mounted = false;
            if(logFile) logFile.close();
            sd.end();
            lcd.print("Card unmounted");
          }else{
            lcd.print("Card not mounted");
          }
          lcd.setCursor(0, 1);
          lcd.print(controls[9]);
        }else{
          ui_state = 0;
          print_main_menu();
        }
      }
      break;
    default:
      break;
  }
}

int ui_timeout = 0;
bool idled = true;

// 1 = left
// 2 = right
// 3 = both
void handle_button_press(int button){
  ui_timeout = UI_TIMEOUT;
  if(idled){
    idled = false;
    print_main_menu();
    return;
  }
  if(!ui_state){ // main menu
    if(button == 3){
      ui_state = 1;
      submenu_keypress(0);
    }else{
      if(button == 1 && menu_index > 0) menu_index--;
      if(button == 2 && menu_index < (menu_size - 1)) menu_index++;
      print_main_menu();
    }
  }else{ // submenu
    submenu_keypress(button);
  }
}

void idle_ui(){
  idled = true;
  int seconds = timeNow.second();
  char logging_char = sd_mounted ? (log_interval > 0 ? '#' : '/') : '!';
  if(seconds % 10 < 5)
    sprintf(print_buf, "%02d:%02d:%02d       %c", timeNow.hour(), timeNow.minute(), seconds, logging_char);
  else
    sprintf(print_buf, "%04d-%02d-%02d     %c", timeNow.year(), timeNow.month(), timeNow.day(), logging_char);
  lcd.home();
  lcd.print(print_buf);
  lcd.setCursor(0, 1);
  sprintf(print_buf, "pH:%d.%02d T:%d.%d   ", (int) pH, two_decimals(pH), (int) temp, one_decimal(temp));
  lcd.print(print_buf);
}


int prev_state = 0;
bool state_reset = false;
void read_buttons(){
  // 0 = no buttons pressed
  // 1 = L pressed
  // 2 = R pressed
  // 3 = both pressed
  int state = (digitalRead(BR) << 1) | digitalRead(BL);
  if(state == prev_state) return;
  if(state_reset && state != 0){
    prev_state = state;
    return;
  }

  if(state_reset){
    state_reset = false;
  }else if(state < prev_state){
    handle_button_press(prev_state);
    if(state != 0) state_reset = true;
  }
  prev_state = state;
}

void log_data(){
  sprintf(print_buf, "%d-%02d-%02d %02d:%02d:%02d,%d,%d.%02d,%d,%d.%d\n",
    timeNow.year(), timeNow.month(), timeNow.day(),
    timeNow.hour(), timeNow.minute(), timeNow.second(),
    pH_voltage, (int) pH, two_decimals(pH),
    temp_voltage, (int) temp, one_decimal(temp)
  );
  logFile.write(print_buf);
  logFile.sync();
}

void setup() {
  pinMode(CLOCK_PIN, INPUT_PULLUP);
  pinMode(BL, INPUT);
  pinMode(BR, INPUT);
  pinMode(PH_PIN, INPUT);
  pinMode(TEMP_PIN, INPUT);
  pinMode(SD_CS, OUTPUT);

  rtc.begin();
  timeNow = rtc.now();

  rtc.writeSqwPinMode(DS1307_SquareWave1HZ);

  lcd.init();
  lcd.createChar(1, up_arrow);
  lcd.createChar(2, down_arrow);
  lcd.backlight();

  lcd.print("Mounting SD card");
  sd_mounted = sd.begin(SD_CS, SPI_HALF_SPEED);
  if(!sd_mounted){
    lcd.clear();
    lcd.home();
    lcd.print("Card not mounted");
    lcd.setCursor(0, 1);
    lcd.print("logging disabled");
    ui_timeout = 5;
  }else{
    openLogFile();
  }

  attachInterrupt(digitalPinToInterrupt(CLOCK_PIN), every_second, FALLING);

  if(ui_timeout == 0) print_main_menu();
}

void loop() {
  read_buttons();

  if(!trigger_second) return;
  trigger_second = false;

  timeNow = rtc.now();

  if(ui_timeout > 0) ui_timeout--;
  if(ui_timeout == 0 && !ui_state) idle_ui();

  pH_voltage = analogRead(PH_PIN);
  pH = ph_from_voltage(pH_voltage);
  
  temp_voltage = analogRead(TEMP_PIN);
  temp = abstemp_cma(temp_voltage);

  if(sd_mounted){
    if(logFileDay != timeNow.day()) openLogFile();
    if(log_interval > 0 && timeNow.unixtime() % log_interval == 0)
      log_data();
  }
}
