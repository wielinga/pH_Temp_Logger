# pH Temp Logger

Arduino Uno pH and Temperature logger using a datalogger shield (SD card and real time clock), custom sensor shield, and an I<sup>2</sup>C LCD.

## Calibration

The pH sensor will require calibration.

To calibrate the sensor you'll need some pH buffer solutions (we used 5, 7 and 10 pH).

Navigate the menu to the "Calibrate pH" entry and press both buttons simultatiously to select it.

Create a spreadsheet and enter the buffer solution pH values of your buffers in the second column. Put the pH probe in the first solution and press the read button until the voltage stabilizes. Enter the voltage in the first column. Do the same for the other solutions.

Create a scatter plot with the voltage readings on the x-axis, and add a linear trend line. Show the function of the trend line and write down the multiplier and addition in the `calibration.h` and `calibration.py` files.

The temperature sensor should have calibration values in its datasheet.

## Operation

The logger has two buttons. After 10 seconds of no input on the main menu, the device will go into its idle state and show the date, time, and sensor readings. Wake it from its idle state by pressing any button. The display shows the functions of the buttons in each specific menu. In menus where the buttons show `<` and `>`, press both buttons simultaniously to select.

## Processing the data

The logger logs the raw analog readings in addition to the calculated values, so that the calibration values can be adjusted after logging without invalidating the data. Processing that data is done with the supplied python script.

If you have multiple files and wish to concatenate them, you can do so with [cat](https://en.wikipedia.org/wiki/Cat_(Unix)).

```sh
cat file1.csv file2.csv > concatenated.csv
```

Simply run the Python script with the CSV file name as an argument, and it will create a processed CSV file with the pH and temperature.

```sh
python3 process.py [input]
```

If you want to average every n readings, add an argument after the input file name.

```sh
python3 process.py [input] [n_average]
```
